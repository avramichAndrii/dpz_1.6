﻿using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;


namespace Library.DAL
{
    public class LibraryContext : DbContext
    {
         public DbSet<Article> Articles { get; set; }

         public DbSet<Comment> Comments { get; set; }

         public DbSet<FormData> FormData { get; set; }


        static LibraryContext()
        {
            Database.SetInitializer<LibraryContext>(new DbInitializer());
        }
        public LibraryContext() : base("DefaultConnection") {}
    }

    public class DbInitializer : DropCreateDatabaseIfModelChanges<LibraryContext>
    {
        protected override void Seed(LibraryContext db)
        {
            db.Articles.Add(new Article { Id=Guid.NewGuid(),Name="SomeName",Description="Desc",Author="aut",Date=DateTime.Now,Image=null });
            db.SaveChanges();
        }
    }
}

