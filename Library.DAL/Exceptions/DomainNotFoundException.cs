﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Exceptions
{
    /// <summary>
    /// Raises when operation is not found
    /// </summary>
    public class DomainNotFoundException : Exception
    {
        private const string CompanyNotFoundMessage = "Item with id = {0} is not found";

        public DomainNotFoundException() : base() { }

        public DomainNotFoundException(Guid id) : base(string.Format(CompanyNotFoundMessage, id)) { }
    }
}
