﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Exceptions
{
    /// <summary>
    /// Raise when operation could not be performed
    /// </summary>
    class InvalidDomainOperationException : Exception
    {
        public InvalidDomainOperationException() : base() { }
    }
}
