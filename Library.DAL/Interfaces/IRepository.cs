﻿using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interfaces
{
    /// <summary>
    /// Generic interface for all repositories
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        Task<IList<T>> GetAllAsync();
        Task<T> GetAsync(Guid id);
        Task CreateAsync(T comment);
        Task<T> UpdateAsync(Guid id, T comment);
        Task DeleteAsync(Guid id);

    }
}
