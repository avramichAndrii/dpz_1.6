﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Library.DAL.Models
{
    public class Comment : BaseEntity
    {
        public string Name { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string User { get; set; }
        [Required]
        public string Body { get; set; }
    }
}
