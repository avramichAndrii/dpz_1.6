﻿using Library.DAL.Exceptions;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    /// <summary>
    /// Comment repository
    /// </summary>
    public class CommentRepository : IRepository<Comment>
    {
        private readonly LibraryContext _libraryContext;

        public CommentRepository()
        {
            _libraryContext = new LibraryContext();

        }
        /// <summary>
        /// Gets all repositories
        /// </summary>
        /// <returns></returns>
        public async Task<IList<Comment>> GetAllAsync()
        {
            return await _libraryContext.Comments.ToListAsync();
        }
        /// <summary>
        /// Gets comment by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Comment> GetAsync(Guid id)
        {
            try
            {
                return await _libraryContext.Comments.SingleOrDefaultAsync(x => x.Id == id);
            }
            catch
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Creates new comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task CreateAsync(Comment comment)
        {
            try
            {
                _libraryContext.Comments.Add(comment);
                await _libraryContext.SaveChangesAsync();
            }
            catch
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Updates existing comment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public async Task<Comment> UpdateAsync(Guid id, Comment comment)
        {
            Comment modelToUpdate = await _libraryContext.Comments.SingleOrDefaultAsync(cm => cm.Id == id);
            if (modelToUpdate == null)
            {
                throw new DomainNotFoundException(id);
            }
            modelToUpdate.Name = comment.Name;
            modelToUpdate.Body = comment.Body;
            modelToUpdate.User = comment.User;
            modelToUpdate.Date = comment.Date;
            try
            {
                await _libraryContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Deletes comment from db
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            Comment comment = await _libraryContext.Comments.SingleOrDefaultAsync(x => x.Id == id);
            if (comment != null)
            {
                try
                {
                    _libraryContext.Comments.Remove(comment);
                }
                catch
                {
                    throw new InvalidDomainOperationException();
                }
            }
        }

    }
}
