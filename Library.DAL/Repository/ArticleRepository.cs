﻿using Library.DAL.Exceptions;
using Library.DAL.Interfaces;
using Library.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    /// <summary>
    /// Article repository
    /// </summary>
    public class ArticleRepository : IRepository<Article>
    {
        private readonly LibraryContext _libraryContext;

        public ArticleRepository()
        {
             _libraryContext = new LibraryContext();
      
        }
        /// <summary>
        /// Gets all articles
        /// </summary>
        /// <returns></returns>
        public async Task<IList<Article>> GetAllAsync()
        {
            return await _libraryContext.Articles.ToListAsync();
        }
        /// <summary>
        /// Gets article by it`s id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Article> GetAsync(Guid id)
        {
            try 
            { 
            return  await _libraryContext.Articles.SingleOrDefaultAsync(x => x.Id == id);
            }
            catch
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Creates article by it`s model
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public async Task CreateAsync(Article book)
        {
            try
            {
                _libraryContext.Articles.Add(book);
                await _libraryContext.SaveChangesAsync();
            }
            catch
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Updates article by it`s id and model
        /// </summary>
        /// <param name="id"></param>
        /// <param name="article"></param>
        /// <returns></returns>
        public async Task<Article> UpdateAsync(Guid id,Article article)
        {
            Article modelToUpdate = await _libraryContext.Articles.SingleOrDefaultAsync(cm => cm.Id == id);
            if (modelToUpdate == null)
            {
                throw new DomainNotFoundException(id);
            }
            modelToUpdate.Name = article.Name;
            modelToUpdate.Description = article.Description;
            modelToUpdate.Author = article.Author;
            modelToUpdate.Date = article.Date;
            modelToUpdate.Image = article.Image;
            try
            {
                await _libraryContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDomainOperationException();
            }
        }
        /// <summary>
        /// Deletes article from bd
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid id)
        {
            Article article = await _libraryContext.Articles.SingleOrDefaultAsync(x => x.Id == id);
            if (article != null)
            {
                try
                {
                    _libraryContext.Articles.Remove(article);
                }
            catch
            {
                throw new InvalidDomainOperationException();
            }
            }
        }

    }
}
