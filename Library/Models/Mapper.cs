﻿using Library.BLL.ModelsDTO;
using System;

namespace Library.Models
{
    /// <summary>
    /// Extension method for mapping
    /// </summary>
    public static class MapperView
    {
        public static ArticleView ToView(this ArticleDTO article)
        {
            return new ArticleView()
            {
                Author = article.Author,
                Date = article.Date,
                Description = article.Description,
                Image = article.Image,
                Name = article.Name
            };
        }

        public static ArticleDTO FromView(this ArticleView article)
        {
            return new ArticleDTO()
            {
                Id = Guid.NewGuid(),
                Author = article.Author,
                Date = article.Date,
                Description = article.Description,
                Image = article.Image,
                Name = article.Name
            };
        }

        public static CommentView ToView(this CommentDTO comment)
        {
            return new CommentView()
            {
                Date = comment.Date,
                User = comment.User,             
                Body = comment.Body
            };
        }

        public static CommentDTO FromView(this CommentView comment)
        {
            return new CommentDTO()
            {
                Id = Guid.NewGuid(),
                Date = comment.Date,
                User = comment.User,
                Body = comment.Body,
                Name = "empty"
            };
        }

        public static FormDataView ToDTO(this FormDataDTO data)
        {
            return new FormDataView()
            {
                Description = data.Description,
                Gender = data.Gender,
                Mark = data.Mark.ToString(),
                Name = data.Name,
                Surname = data.Surname
            };
        }

        public static FormDataDTO FromView(this FormDataView data)
        {
            return new FormDataDTO()
            {
                Description = data.Description,
                Gender = data.Gender,
                Mark = Convert.ToInt32(data.Mark),
                Name = data.Name,
                Surname = data.Surname
            };
        }
    }
}