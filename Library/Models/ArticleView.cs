﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Library.Models
{
    /// <summary>
    /// View model for arcticle
    /// </summary>
    public class ArticleView
    {
        [DataType(DataType.Text)]
        public string Name { get; set; }
        [DataType(DataType.Text)]
        public string Author { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required]
        public string Description { get; set; }
        public byte[] Image { get; set; }
    }
}