﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Models
{
    /// <summary>
    /// Comment view model
    /// </summary>
    public class CommentView
    {
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required]
        public string User { get; set; }
        [Required]
        public string Body { get; set; }
    }
}