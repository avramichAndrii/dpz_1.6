﻿using Library.BLL.Services;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    /// <summary>
    /// Form controller that gets user review and displays it
    /// </summary>
    public class FormController : Controller
    {
        private readonly FormDataService _dataService;

        public FormController()
        {
            _dataService = new FormDataService();
        }

        public ActionResult Form()
        {

            List<string> listCheck = new List<string>() { "Male", "Female" };
            ViewBag.listCheck = listCheck;
            string[] list = new string[] { "1", "2", "3","4","5" };
            ViewBag.list = list;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> FormResult(string name, string surname, string radio, string opt,string txt)
        {
            FormDataView view = new FormDataView()
            {
                Description = txt,
                Gender = radio,
                Mark = opt.ToString(),
                Name = name,
                Surname = surname
            };
            await  _dataService.CreateAsync(view.FromView());
            ViewBag.name = view.Name;
            ViewBag.surname = view.Surname;
            ViewBag.radio = view.Gender;
            ViewBag.opt = view.Mark;
            ViewBag.txt = view.Description;
            return View();
        }
    }
}
