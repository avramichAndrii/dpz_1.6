﻿using Library.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Test.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Home()
        {
            HomeController controller = new HomeController();

            // Act
            Task<ActionResult> result = controller.Main();

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
