﻿using Library.BLL.Mappers;
using Library.BLL.ModelsDTO;
using Library.DAL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Tests.Mapping
{
    [TestClass]
    public class MapperTest
    {
        [TestMethod]
        public void ArticleToDtoTest()
        {
            Article article = new Article() { Author = "Test", Date = DateTime.Now, Description = "some", Id = Guid.NewGuid(), Image = null, Name = "Bob" };
            ArticleDTO target = article.ToDTO();

            Assert.AreEqual(article.Name, target.Name);
            Assert.AreEqual(article.Author, target.Author);
            Assert.AreEqual(article.Date, target.Date);
        }

        [TestMethod]
        public void ArticleFromDTOTest()
        {
            ArticleDTO article = new ArticleDTO() { Author = "Test", Date = DateTime.Now, Description = "some", Id = Guid.NewGuid(), Image = null, Name = "Bob" };
            Article target = article.FromDTO();

            Assert.AreEqual(article.Name, target.Name);
            Assert.AreEqual(article.Author, target.Author);
            Assert.AreEqual(article.Date, target.Date);
        }
        [TestMethod]
        public void CommentToDTOTest()
        {
            Comment comment = new Comment() { Id = Guid.NewGuid(), Body = "test", Date = DateTime.Now, Name = "tst", User = "vasil" };

            CommentDTO target = comment.ToDTO();

            Assert.AreEqual(comment.Id, target.Id);
            Assert.AreEqual(comment.Name, target.Name);
            Assert.AreEqual(comment.User, target.User);
        }
        [TestMethod]
        public void CommentFromDTOTest()
        {
            CommentDTO comment = new CommentDTO() { Id = Guid.NewGuid(), Body = "test", Date = DateTime.Now, Name = "tst", User = "vasil" };

            Comment target = comment.FromDTO();

            Assert.AreEqual(comment.Id, target.Id);
            Assert.AreEqual(comment.Name, target.Name);
            Assert.AreEqual(comment.User, target.User);
        }
        [TestMethod]
        public void FormDataToDTOTest()
        {
            FormData data = new FormData() { Id = Guid.NewGuid(), Description = "test", Gender = "Male", Mark = 4, Name = "Vasil", Surname = "Brown" };

            FormDataDTO dataDTO = data.ToDTO();

            Assert.AreEqual(data.Description, dataDTO.Description);
            Assert.AreEqual(data.Gender, dataDTO.Gender);
            Assert.AreEqual(data.Name, dataDTO.Name);
        }
        [TestMethod]
        public void FormDataFromDTOTest()
        {
            FormDataDTO data = new FormDataDTO() {Description = "test", Gender = "Male", Mark = 4, Name = "Vasil", Surname = "Brown" };

            FormData dataDTO = data.FromDTO();

            Assert.AreEqual(data.Description, dataDTO.Description);
            Assert.AreEqual(data.Gender, dataDTO.Gender);
            Assert.AreEqual(data.Name, dataDTO.Name);
        }
    }
}
