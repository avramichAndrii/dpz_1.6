﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.ModelsDTO
{
    /// <summary>
    /// DTO class for form data
    /// </summary>
    public class FormDataDTO
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public int Mark { get; set; }
        public string Description { get; set; }
    }
}
