﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL.ModelsDTO
{
    /// <summary>
    /// DTO class for articles
    /// </summary>
    public class ArticleDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
    }
}
