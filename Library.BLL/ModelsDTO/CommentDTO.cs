﻿using System;

namespace Library.BLL.ModelsDTO
{
    /// <summary>
    /// DTO class for comments
    /// </summary>
    public class CommentDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string User { get; set; }
        public string Body { get; set; }
    }
}
