﻿using Library.BLL.Mappers;
using Library.BLL.ModelsDTO;
using Library.DAL.Models;
using Library.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    /// <summary>
    /// Service for articles 
    /// </summary>
    public class ArticleService
    {
        private readonly ArticleRepository _articleRepository;
        public ArticleService()
        {
            _articleRepository = new ArticleRepository();
        }

        public async Task<IList<ArticleDTO>> GetAllAsync()
        {
            List<ArticleDTO> articles = new List<ArticleDTO>();
            foreach (var i in await _articleRepository.GetAllAsync())
            {
                articles.Add(i.ToDTO());
            }
            return articles;
        }

        public async Task<ArticleDTO> GetArticleAsync(Guid id)
        {
            var tmp = await _articleRepository.GetAsync(id);
            return tmp.ToDTO();
        }

        public async Task CreateAsync(ArticleDTO article)
        {
            await _articleRepository.CreateAsync(article.FromDTO());
        }

        public async Task<ArticleDTO> UpdateAsync(Guid id, ArticleDTO article)
        {
            var tmp = await _articleRepository.UpdateAsync(id, article.FromDTO());
            return tmp.ToDTO();
        }

        public async Task DeleteAsync(Guid id)
        {
            await _articleRepository.DeleteAsync(id);
        }
    }
}
