﻿using Library.BLL.Mappers;
using Library.BLL.ModelsDTO;
using Library.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.BLL.Services
{
    /// <summary>
    /// Service for formdata classes
    /// </summary>
    public class FormDataService
    {
        private readonly FormDataRepository _dataRepository;
        public FormDataService()
        {
            _dataRepository = new FormDataRepository();
        }

        public async Task<IList<FormDataDTO>> GetAllAsync()
        {
            List<FormDataDTO> data = new List<FormDataDTO>();
            foreach (var i in await _dataRepository.GetAllAsync())
            {
                data.Add(i.ToDTO());
            }
            return data;
        }

        public async Task<FormDataDTO> GetArticleAsync(Guid id)
        {
            var tmp = await _dataRepository.GetAsync(id);
            return tmp.ToDTO();
        }

        public async Task CreateAsync(FormDataDTO data)
        {
            await _dataRepository.CreateAsync(data.FromDTO());
        }

        public async Task<FormDataDTO> UpdateAsync(Guid id, FormDataDTO data)
        {
            var tmp = await _dataRepository.UpdateAsync(id, data.FromDTO());
            return tmp.ToDTO();
        }

        public async Task DeleteAsync(Guid id)
        {
            await _dataRepository.DeleteAsync(id);
        }
    }
}
